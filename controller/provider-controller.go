package controller

import (
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/moon1139/go-gin-elastic/entity"
	"gitlab.com/moon1139/go-gin-elastic/repository"
	"gitlab.com/moon1139/go-gin-elastic/service"

	"gopkg.in/go-playground/validator.v9"
)

//ProviderController bla
type ProviderController interface {
	FindAll(ctx *gin.Context) map[string]interface{}
	Save(ctx *gin.Context) map[string]interface{}
	Update(ctx *gin.Context) error
	Delete(ctx *gin.Context) error
	//ShowAll(ctx *gin.Context)
}

type controller struct {
	service service.ProviderService
}

var validate *validator.Validate

//New bla
func New(service service.ProviderService) ProviderController {
	validate = validator.New()
	return &controller{
		service: service,
	}
}

// func (c *controller) FindAll() []entity.Provider {
// 	return c.service.FindAll()
// }

// func (c *controller) FindAll(ctx *gin.Context) map[string]interface{} {
// 	fmt.Println("In controller gin.AuthUserKey: ")
// 	fmt.Println(ctx.Get(gin.AuthUserKey))
// 	return c.service.FindAll(ctx)
// }

func (c *controller) FindAll(ctx *gin.Context) map[string]interface{} {
	return repository.Get(ctx)
}

func (c *controller) Save(ctx *gin.Context) map[string]interface{} {
	var provider entity.Provider
	err := ctx.ShouldBindJSON(&provider)
	if err != nil {
		fmt.Println("Json format fail")
		//return err
	}
	err = validate.Struct(provider)
	if err != nil {
		fmt.Println("bad provider struct")
		//return err
	}
	return c.service.Save(ctx, provider)
	// return nil
}

func (c *controller) Update(ctx *gin.Context) error {
	var provider entity.Provider
	err := ctx.ShouldBindJSON(&provider)
	if err != nil {
		return err
	}

	id, err := strconv.ParseUint(ctx.Param("id"), 0, 0)
	if err != nil {
		return err
	}
	provider.ID = id

	err = validate.Struct(provider)
	if err != nil {
		return err
	}
	c.service.Update(provider)
	return nil
}

func (c *controller) Delete(ctx *gin.Context) error {
	var provider entity.Provider
	id, err := strconv.ParseUint(ctx.Param("id"), 0, 0)
	if err != nil {
		return err
	}
	provider.ID = id
	c.service.Delete(provider)
	return nil
}

// func (c *controller) ShowAll(ctx *gin.Context) {
// 	providers := c.service.FindAll()
// 	data := gin.H{
// 		"title":  "Provider List",
// 		"providers": providers,
// 	}
// 	ctx.HTML(http.StatusOK, "index.html", data)
// }

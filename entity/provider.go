package entity

import "time"

type Provider struct {
	ID        uint64    `gorm:"primary_key;auto_increment" json:"id"`
	FirstName string    `json:"firstName" binding:"required" gorm:"type:varchar(100)"`
	LastName  string    `json:"lastName" binding:"required" gorm:"type:varchar(100)"`
	CreatedAt time.Time `json:"-" gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `json:"-" gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

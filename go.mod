module gitlab.com/moon1139/go-gin-elastic

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/gorm v1.9.15
	github.com/stretchr/testify v1.6.1
	gopkg.in/go-playground/validator.v9 v9.31.0
)

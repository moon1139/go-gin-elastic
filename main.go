package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/moon1139/go-gin-elastic/accounts"
	"gitlab.com/moon1139/go-gin-elastic/controller"
	"gitlab.com/moon1139/go-gin-elastic/repository"
	"gitlab.com/moon1139/go-gin-elastic/service"
)

var (
	providerRepository repository.ProviderRepository = repository.NewProviderRepository()
	providerService    service.ProviderService       = service.New(providerRepository)
	providerController controller.ProviderController = controller.New(providerService)
)

func main() {
	defer providerRepository.CloseDB()

	router := SetupRouter()

	router.Run(":8080")
}

// SetupRouter is blablabla
func SetupRouter() *gin.Engine {

	// server := gin.New()
	// server.Use(gin.Recovery(), gin.Logger())

	router := gin.Default()
	//router.Use(gin.Logger())

	account := accounts.GetJSONInfo()
	user := gin.Accounts{account.User: account.Password}
	authorized := router.Group("/v1", gin.BasicAuth(user))

	authorized.GET("/", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{
			"hello": "world",
		})
	})

	authorized.GET("/providers", func(ctx *gin.Context) {
		// fmt.Println("In main.get gin.AuthUserKey: ")
		// fmt.Println(ctx.Get(gin.AuthUserKey))
		ctx.JSON(200, providerController.FindAll(ctx))
	})

	// authorized.GET("/providers/:id", func(ctx *gin.Context) {
	// 	fmt.Println("In main.get gin.AuthUserKey: ")
	// 	fmt.Println(ctx.Get(gin.AuthUserKey))
	// 	ctx.JSON(200, providerController.FindAll(ctx))
	// })

	authorized.POST("/providers", func(ctx *gin.Context) {
		// err := providerController.Save(ctx)
		// if err != nil {
		// 	ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		// } else {
		// 	ctx.JSON(http.StatusOK, gin.H{"message": "Success!"})
		// }
		ctx.JSON(200, providerController.Save(ctx))
	})

	authorized.PUT("/providers/:id", func(ctx *gin.Context) {
		err := providerController.Update(ctx)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		} else {
			ctx.JSON(http.StatusOK, gin.H{"message": "Success!"})
		}

	})

	authorized.DELETE("/providers/:id", func(ctx *gin.Context) {
		err := providerController.Delete(ctx)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		} else {
			ctx.JSON(http.StatusOK, gin.H{"message": "Success!"})
		}

	})

	return router
}

package repository

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/moon1139/go-gin-elastic/accounts"
	"gitlab.com/moon1139/go-gin-elastic/entity"
)

const (
	domain = "https://search-revex-es-jtest-ju7sjeitdlcgth3vbdql2cfnmm.us-east-1.es.amazonaws.com"
	index  = "providers"
)

//ElasticRepository bla
// type ElasticRepository interface {
// 	Get(c *gin.Context) map[string]interface{}
// }

// Get ...
func Get(c *gin.Context) map[string]interface{} {
	//userID := c.Param("id")
	// Basic information for the Amazon Elasticsearch Service domain
	//endpoint := domain + "/" + index + "/" + "_search" + "/" + "?q=" + userID
	endpoint := domain + "/" + index + "/" + "_search"

	fmt.Println("endpoint is :")
	fmt.Println(endpoint)

	client := &http.Client{}

	// Form the HTTP request
	req, err := http.NewRequest(http.MethodGet, endpoint, nil)

	basicAuth(c, req)

	if err != nil {
		c.JSON(http.StatusOK, gin.H{"message": "invalid authorization"})
		return nil
	}

	resp, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": "invalid user id"})
		return nil
	}

	bodyText, err := ioutil.ReadAll(resp.Body)

	// fmt.Println("body text is: ")
	// fmt.Println(bodyText)

	var jsonResult map[string]interface{}
	err = json.Unmarshal(bodyText, &jsonResult)

	fmt.Println("jsonResult is: ")
	fmt.Println(jsonResult)

	if err != nil {
		fmt.Println("Unmarshal fail")
	}

	return jsonResult
}

// Post ...
func Post(c *gin.Context, provider entity.Provider) map[string]interface{} {
	endpoint := domain + "/" + index + "/" + "_doc"

	fmt.Println("endpoint is :")
	fmt.Println(endpoint)

	client := &http.Client{}

	jsondata, _ := json.Marshal(provider)
	b := bytes.NewBuffer(jsondata)
	fmt.Println("jsondata: ")
	fmt.Println(jsondata)
	fmt.Println("b: ")
	fmt.Println(b)

	// Form the HTTP request
	req, err := http.NewRequest(http.MethodPost, endpoint, b)
	req.Header.Add("accept", "application/json")
	req.Header.Add("content-type", "application/json")

	basicAuth(c, req)

	if err != nil {
		c.JSON(http.StatusOK, gin.H{"message": "invalid authorization"})
		return nil
	}

	resp, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": "invalid user id"})
		return nil
	}

	bodyText, err := ioutil.ReadAll(resp.Body)

	// fmt.Println("body text is: ")
	// fmt.Println(bodyText)

	var jsonResult map[string]interface{}
	err = json.Unmarshal(bodyText, &jsonResult)

	fmt.Println("jsonResult is: ")
	fmt.Println(jsonResult)

	if err != nil {
		fmt.Println("Unmarshal fail")
	}

	return jsonResult
}

func basicAuth(c *gin.Context, req *http.Request) {
	account := accounts.GetJSONInfo()

	accounts := gin.Accounts{
		account.User: account.Password,
	}

	secrets := gin.H{account.User: account.Email}

	//fmt.Println("secrets: ")
	//fmt.Println(secrets)
	// fmt.Println("gin.Context: ")
	// fmt.Println(c)
	// fmt.Println("gin.AuthUserKey: ")
	// fmt.Println(c.Get(gin.AuthUserKey))

	user := c.MustGet(gin.AuthUserKey).(string)

	if _, ok := secrets[user]; !ok {
		c.JSON(http.StatusOK, gin.H{"message": "invalid authorization"})
		return
	}

	req.SetBasicAuth(user, accounts[user])
}

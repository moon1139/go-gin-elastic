package repository

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite" //warning shutup
	"gitlab.com/moon1139/go-gin-elastic/entity"
)

//ProviderRepository shutup warning
type ProviderRepository interface {
	Save(ctx *gin.Context, Provider entity.Provider) map[string]interface{}
	Update(Provider entity.Provider)
	Delete(Provider entity.Provider)
	FindAll(ctx *gin.Context) map[string]interface{}
	CloseDB()
}

type database struct {
	connection *gorm.DB
}

//NewProviderRepository shutup warning
func NewProviderRepository() ProviderRepository {
	db, err := gorm.Open("sqlite3", "test.db")
	if err != nil {
		panic("Failed to connect database")
	}
	db.AutoMigrate(&entity.Provider{})
	return &database{
		connection: db,
	}
}

func (db *database) CloseDB() {
	err := db.connection.Close()
	if err != nil {
		panic("Failed to close database")
	}
}

func (db *database) Save(ctx *gin.Context, provider entity.Provider) map[string]interface{} {
	fmt.Println("In repository Save")
	res := Post(ctx, provider)
	return res
}

func (db *database) Update(provider entity.Provider) {
	db.connection.Save(&provider)
}

func (db *database) Delete(provider entity.Provider) {
	db.connection.Delete(&provider)
}

// func (db *database) FindAll() []entity.Provider {
// 	var providers []entity.Provider
// 	db.connection.Set("gorm:auto_preload", true).Find(&providers)
// 	return providers
// }

// FindAll ...
func (db *database) FindAll(ctx *gin.Context) map[string]interface{} {
	fmt.Println("In repository FindAll")
	res := Get(ctx)
	return res
}

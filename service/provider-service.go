package service

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/moon1139/go-gin-elastic/entity"
	"gitlab.com/moon1139/go-gin-elastic/repository"
)

//ProviderService warning shutup
type ProviderService interface {
	Save(ctx *gin.Context, provider entity.Provider) map[string]interface{}
	Update(entity.Provider) error
	Delete(entity.Provider) error
	FindAll(ctx *gin.Context) map[string]interface{}
}

type providerService struct {
	repository repository.ProviderRepository
}

//New new a service
func New(providerRepository repository.ProviderRepository) ProviderService {
	return &providerService{
		repository: providerRepository,
	}
}

func (service *providerService) Save(ctx *gin.Context, provider entity.Provider) map[string]interface{} {
	return service.repository.Save(ctx, provider)
	// return nil
}

func (service *providerService) Update(provider entity.Provider) error {
	service.repository.Update(provider)
	return nil
}

func (service *providerService) Delete(provider entity.Provider) error {
	service.repository.Delete(provider)
	return nil
}

func (service *providerService) FindAll(ctx *gin.Context) map[string]interface{} {
	return service.repository.FindAll(ctx)
}
